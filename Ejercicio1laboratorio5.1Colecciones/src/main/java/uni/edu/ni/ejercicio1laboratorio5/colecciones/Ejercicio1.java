/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.edu.ni.ejercicio1laboratorio5.colecciones;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *La UNI necesita registrar los nombres de todos sus empleados.
 * Cada mes, un empleado será seleccionado aleatoriamente del registro y recibirá una beca para estudiar
 * @author Sistemas36
 */
public class Ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       List <String> empleados= new ArrayList<>();
       Scanner md= new Scanner(System.in);
       int opcion, salida=0;
       do{
           do{
                System.out.println("Digite la opcion que usted necesite");
                System.out.println("1. Registrar Empleado");
                System.out.println("2. Seleccionar Empleado aleatorio del mes para la beca");
                System.out.println("3. Salir");  
                opcion=md.nextInt();
                if(opcion>3 && opcion<1){
                    System.out.println("ERROR");
                    System.out.println("Ponga un numero que este dentro de las variables");
                }else{
                    salida = 1;
                }
            }while(salida==0 );
                switch(opcion){
                    case 1:
                        System.out.println("Digite el nombre que desea agregar");
                        String nombredeempleado = md.next();
                        empleados.add(nombredeempleado);
                        System.out.println("___________________");
                        break;
                    case 2:
                        
                            Random n = new Random();
                            int numero = n.nextInt(empleados.size());
                            System.out.println("Empleado elegido: " + empleados.get(numero));
                            System.out.println("_______________");
                        
                        break;
                    case 3:
                        System.out.println("Byeee");
                        salida=2;
                        break;
                }
    }while(salida!=2);
    
}
}
