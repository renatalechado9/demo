/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import ni.edu.uni.programacion1.util.Validate;
import static ni.edu.uni.programacion1.util.Validate.validateIsInteger;
import ni.edu.uni.programacion1.enums.Sexo;
/**
 *
 * @author Sistemas36
 */
public class ConsoleReader {
    public static int readInt(BufferedReader reader,String message) throws IOException{
        boolean flag = true;
        String text ="";
        do{
            System.out.println(message);
            text = reader.readLine();
            flag = validateIsInteger(text);
        }while(!flag);
        return Integer.parseInt(text);
        
    }
    
    public static String readLetter(BufferedReader reader ,String message) throws IOException{
        boolean flag = true;
        String text ="";
        do{
            System.out.println(message);
            text = reader.readLine();
            flag = Validate.validateIsLetters(text);
        }while(!flag);
        return text;
    }
    
    public static String readCedula(BufferedReader reader,String message) throws IOException{
        boolean flag = true;
        String text ="";
        do{
            System.out.println(message);
            text = reader.readLine();
            flag = Validate.validateIsCedula(text);
        }while(!flag);
        return text;
    }
   public static String readTelefono(BufferedReader reader,String message) throws IOException{
        boolean flag = true;
        String text ="";
        do{
            System.out.println(message);
            text = reader.readLine();
            flag = Validate.validateIsPhoneNumber(text);
        }while(!flag);
        return text;
    }
    public static String readSexo(BufferedReader reader,String message) throws IOException{
        boolean flag = true;
        String text ="";
        String sexo = "";
        do{
            System.out.println(message);
            text = reader.readLine();
            flag = Validate.validateIsSexo(text);
        }while(!flag);
        int numEntero = Integer.parseInt(text);
        if(numEntero==1){
            sexo = "femenino";
        }else if(numEntero==2){
           sexo="Masculino";
        }
        text=sexo;
        return text;
    }    

    public static String readnivelAcademico(BufferedReader reader,String message) throws IOException{
        boolean flag = true;
        String text ="";
        String nivelAcademico="";
        do{
            System.out.println(message);
            text = reader.readLine();
            flag = Validate.validateIsSexo(text);
        }while(!flag);
        int numEntero = Integer.parseInt(text);
        switch(numEntero){
            case 1: 
                nivelAcademico = "Primaria";
            case 2:
                nivelAcademico ="BACHILLER";
            case 3:
                nivelAcademico ="Tecnico Medio";
            case 4:
                nivelAcademico ="Tecnico Superior";
            case 5:
                nivelAcademico ="Licenciado";
            case 6:
                nivelAcademico ="Ingeniero";
            case 7:
                nivelAcademico ="Especialista";
            case 8: 
                nivelAcademico ="Maestria";
            case 9:
                nivelAcademico ="Doctorado";
            case 10:
                nivelAcademico ="POst Doctorado";
        }
        text=nivelAcademico;
        return text;
    }
    
}
