/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import java.util.Arrays;
import ni.edu.uni.programacion1.pojo.Departamento;
import ni.edu.uni.programacion1.pojo.Municipio;

/**
 *
 * @author USUARIO
 */
public class MunicipioData {
    private DepartamentoData departamentosData;
    private Municipio[] municipios;

    public MunicipioData() {
        departamentosData = new DepartamentoData();
        populateMunicipio();
    }
    private void populateMunicipio(){
       municipios = new Municipio[]{
        new Municipio(1,"Boaco",departamentosData.getDepartamentoById(1)),
        new Municipio(2,"Camoapa",departamentosData.getDepartamentoById(1)),
        new Municipio(3,"San Lorenzo",departamentosData.getDepartamentoById(1)),
        new Municipio(4,"San Jose de los Remates",departamentosData.getDepartamentoById(1)),
        new Municipio(5,"Santa Lucia",departamentosData.getDepartamentoById(1)),
        new Municipio(6,"Teustepe",departamentosData.getDepartamentoById(1)),
        new Municipio(7,"Diriambe",departamentosData.getDepartamentoById(2)),
        new Municipio(8,"Dolores",departamentosData.getDepartamentoById(2)),
        new Municipio(9,"El Rosario",departamentosData.getDepartamentoById(2)),
        new Municipio(10,"Jinotepe",departamentosData.getDepartamentoById(2)),
        new Municipio(11,"La Conquista",departamentosData.getDepartamentoById(2)),
        new Municipio(12,"La Paz de Oriente",departamentosData.getDepartamentoById(2)),
        new Municipio(13,"San Marcos",departamentosData.getDepartamentoById(2)),
        new Municipio(14,"Santa Teresa",departamentosData.getDepartamentoById(2)),
        new Municipio(15,"Chichigalpa",departamentosData.getDepartamentoById(3)),
        new Municipio(16,"Chinandega",departamentosData.getDepartamentoById(3)),
        new Municipio(17,"Cinco Pinos",departamentosData.getDepartamentoById(3)),
        new Municipio(18,"Corinto",departamentosData.getDepartamentoById(3)),
        new Municipio(19,"El Realejo",departamentosData.getDepartamentoById(3)),
        new Municipio(20,"El Viejo",departamentosData.getDepartamentoById(3)),
        new Municipio(21,"Posoltega",departamentosData.getDepartamentoById(3)),
        new Municipio(22,"San Francisco del Norte",departamentosData.getDepartamentoById(3)),
        new Municipio(23,"San Pedro del Norte",departamentosData.getDepartamentoById(3)),
        new Municipio(24,"Santo Tomas del Norte",departamentosData.getDepartamentoById(3)),
        new Municipio(25,"Somotillo",departamentosData.getDepartamentoById(3)),
        new Municipio(26,"Puerto Morazan",departamentosData.getDepartamentoById(3)),
        new Municipio(27,"Villanueva",departamentosData.getDepartamentoById(3)),
        new Municipio(28,"Acoyapa",departamentosData.getDepartamentoById(4)),
        new Municipio(29,"Comalapa",departamentosData.getDepartamentoById(4)),
        new Municipio(30,"San Francisco de Cuapa",departamentosData.getDepartamentoById(4)),
        new Municipio(31,"El Coral",departamentosData.getDepartamentoById(4)),
        new Municipio(32,"Juigalpa",departamentosData.getDepartamentoById(4)),
        new Municipio(33,"La Libertad",departamentosData.getDepartamentoById(4)),
        new Municipio(34,"San Pedro de Lovago",departamentosData.getDepartamentoById(4)),
        new Municipio(35,"Santo Domingo",departamentosData.getDepartamentoById(4)),
        new Municipio(36,"Santo Tomas",departamentosData.getDepartamentoById(4)),
        new Municipio(37,"Villa Sandino",departamentosData.getDepartamentoById(4)),
        new Municipio(38,"Puerto Cabezas",departamentosData.getDepartamentoById(5)),
        new Municipio(39,"Bonanza",departamentosData.getDepartamentoById(5)),
        new Municipio(40,"Mulukuku",departamentosData.getDepartamentoById(5)),
        new Municipio(41,"Prinzapolka",departamentosData.getDepartamentoById(5)),
        new Municipio(42,"Rosita",departamentosData.getDepartamentoById(5)),
        new Municipio(43,"Siuna",departamentosData.getDepartamentoById(5)),
        new Municipio(44,"Waslala",departamentosData.getDepartamentoById(5)),
        new Municipio(45,"Waspan",departamentosData.getDepartamentoById(5)),
        new Municipio(46,"Bluefields",departamentosData.getDepartamentoById(6)),
        new Municipio(47,"Desembocadura de Rio Grande",departamentosData.getDepartamentoById(6)),
        new Municipio(48,"El Ayote",departamentosData.getDepartamentoById(6)),
        new Municipio(49,"El Rama",departamentosData.getDepartamentoById(6)),
        new Municipio(50,"El Tortuguero",departamentosData.getDepartamentoById(6)),
        new Municipio(51,"Islas del Maiz",departamentosData.getDepartamentoById(6)),
        new Municipio(52,"Kukra Hill",departamentosData.getDepartamentoById(6)),
        new Municipio(53,"La Cruz de Rio Grande",departamentosData.getDepartamentoById(6)),
        new Municipio(54,"Laguna de Perlas",departamentosData.getDepartamentoById(6)),
        new Municipio(55,"Muelle de los Bueyes",departamentosData.getDepartamentoById(6)),
        new Municipio(56,"Nueva Guinea",departamentosData.getDepartamentoById(6)),
        new Municipio(57,"Paiwas",departamentosData.getDepartamentoById(6)),
        new Municipio(58,"Condega",departamentosData.getDepartamentoById(7)),
        new Municipio(59,"Esteli",departamentosData.getDepartamentoById(7)),
        new Municipio(60,"La Trinidad",departamentosData.getDepartamentoById(7)),
        new Municipio(61,"Pueblo Nuevo",departamentosData.getDepartamentoById(7)),
        new Municipio(62,"San Juan de Limay",departamentosData.getDepartamentoById(7)),
        new Municipio(63,"San Nicolas",departamentosData.getDepartamentoById(7)),
        new Municipio(64,"Diria",departamentosData.getDepartamentoById(8)),
        new Municipio(65,"Diriomo",departamentosData.getDepartamentoById(8)),
        new Municipio(66,"Granada",departamentosData.getDepartamentoById(8)),
        new Municipio(67,"Nandaime",departamentosData.getDepartamentoById(8)),
        new Municipio(68,"El Cua",departamentosData.getDepartamentoById(9)),
        new Municipio(69,"Jinotega",departamentosData.getDepartamentoById(9)),
        new Municipio(70,"La Concordia",departamentosData.getDepartamentoById(9)),
        new Municipio(71,"San Jose de Bocay",departamentosData.getDepartamentoById(9)),
        new Municipio(72,"San Rafael del Norte",departamentosData.getDepartamentoById(9)),
        new Municipio(73,"San Sebastian de Yali",departamentosData.getDepartamentoById(9)),
        new Municipio(74,"Santa Maria de Pantasma",departamentosData.getDepartamentoById(9)),
        new Municipio(75,"Wiwili de Jinotega",departamentosData.getDepartamentoById(9)),
        new Municipio(76,"Achuapa",departamentosData.getDepartamentoById(10)),
        new Municipio(77,"El Jicaral",departamentosData.getDepartamentoById(10)),
        new Municipio(78,"El Sauce",departamentosData.getDepartamentoById(10)),
        new Municipio(79,"La Paz Centro",departamentosData.getDepartamentoById(10)),
        new Municipio(80,"Larreynaga",departamentosData.getDepartamentoById(10)),
        new Municipio(81,"Leon",departamentosData.getDepartamentoById(10)),
        new Municipio(82,"Nagarote",departamentosData.getDepartamentoById(10)),
        new Municipio(83,"Quezalguaque",departamentosData.getDepartamentoById(10)),
        new Municipio(84,"Santa Rosa del Penon",departamentosData.getDepartamentoById(10)),
        new Municipio(85,"Telica",departamentosData.getDepartamentoById(10)),
        new Municipio(86,"Las Sabanas",departamentosData.getDepartamentoById(11)),
        new Municipio(87,"Palacaguina",departamentosData.getDepartamentoById(11)),
        new Municipio(88,"San Jose de Cusmapa",departamentosData.getDepartamentoById(11)),
        new Municipio(89,"San Juan de Rio Coco",departamentosData.getDepartamentoById(11)),
        new Municipio(90,"San Lucas",departamentosData.getDepartamentoById(11)),
        new Municipio(91,"Somoto",departamentosData.getDepartamentoById(11)),
        new Municipio(92,"Telpaneca",departamentosData.getDepartamentoById(11)),
        new Municipio(93,"Totogalpa",departamentosData.getDepartamentoById(11)),
        new Municipio(94,"Yalaguina",departamentosData.getDepartamentoById(11)),
        new Municipio(95,"Ciudad Sandino",departamentosData.getDepartamentoById(12)),
        new Municipio(96,"El Crucero",departamentosData.getDepartamentoById(12)),
        new Municipio(97,"Managua",departamentosData.getDepartamentoById(12)),
        new Municipio(98,"Mateare",departamentosData.getDepartamentoById(12)),
        new Municipio(99,"San Francisco Libre",departamentosData.getDepartamentoById(12)),
        new Municipio(100,"San Rafael del Sur",departamentosData.getDepartamentoById(12)),
        new Municipio(101,"Ticuantepe",departamentosData.getDepartamentoById(12)),
        new Municipio(102,"Tipitapa",departamentosData.getDepartamentoById(12)),
        new Municipio(103,"Villa El Carmen",departamentosData.getDepartamentoById(12)),
        new Municipio(104,"Catarina",departamentosData.getDepartamentoById(13)),
        new Municipio(105,"La Concepcion",departamentosData.getDepartamentoById(13)),
        new Municipio(106,"Masatepe",departamentosData.getDepartamentoById(13)),
        new Municipio(107,"Masaya",departamentosData.getDepartamentoById(13)),
        new Municipio(108,"Nandasmo",departamentosData.getDepartamentoById(13)),
        new Municipio(109,"Nindiri",departamentosData.getDepartamentoById(13)),
        new Municipio(110,"Niquinohomo",departamentosData.getDepartamentoById(13)),
        new Municipio(111,"San Juan de Oriente",departamentosData.getDepartamentoById(13)),
        new Municipio(112,"Tisma",departamentosData.getDepartamentoById(13)),
        new Municipio(113,"Ciudad Dario",departamentosData.getDepartamentoById(14)),
        new Municipio(114,"El Tuma - La Dalia",departamentosData.getDepartamentoById(14)),
        new Municipio(115,"Esquipulas",departamentosData.getDepartamentoById(14)),
        new Municipio(116,"Matagalpa",departamentosData.getDepartamentoById(14)),
        new Municipio(117,"Matiguas",departamentosData.getDepartamentoById(14)),
        new Municipio(118,"Muy Muy",departamentosData.getDepartamentoById(14)),
        new Municipio(119,"Rancho Grande",departamentosData.getDepartamentoById(14)),
        new Municipio(120,"Rio Blanco",departamentosData.getDepartamentoById(14)),
        new Municipio(121,"San Dionisio",departamentosData.getDepartamentoById(14)),
        new Municipio(122,"San Isidro",departamentosData.getDepartamentoById(14)),
        new Municipio(123,"San Ramon",departamentosData.getDepartamentoById(14)),
        new Municipio(124,"Sebaco",departamentosData.getDepartamentoById(14)),
        new Municipio(125,"Terrabona",departamentosData.getDepartamentoById(14)),
        new Municipio(126,"Ciudad Antigua",departamentosData.getDepartamentoById(15)),
        new Municipio(127,"Dipilto",departamentosData.getDepartamentoById(15)),
        new Municipio(128,"El Jicaro",departamentosData.getDepartamentoById(15)),
        new Municipio(129,"Guiguili",departamentosData.getDepartamentoById(15)),
        new Municipio(130,"Jalapa",departamentosData.getDepartamentoById(15)),
        new Municipio(131,"Macuelizo",departamentosData.getDepartamentoById(15)),
        new Municipio(132,"Mozonte",departamentosData.getDepartamentoById(15)),
        new Municipio(133,"Murra",departamentosData.getDepartamentoById(15)),
        new Municipio(134,"Ocotal",departamentosData.getDepartamentoById(15)),
        new Municipio(135,"Quilali",departamentosData.getDepartamentoById(15)),
        new Municipio(136,"San Fernando",departamentosData.getDepartamentoById(15)),
        new Municipio(137,"Santa Maria",departamentosData.getDepartamentoById(15)),
        new Municipio(138,"El Almendro",departamentosData.getDepartamentoById(16)),
        new Municipio(139,"El Castillo",departamentosData.getDepartamentoById(16)),
        new Municipio(140,"Morrito",departamentosData.getDepartamentoById(16)),
        new Municipio(141,"San Carlos",departamentosData.getDepartamentoById(16)),
        new Municipio(142,"San Juan del Norte",departamentosData.getDepartamentoById(16)),
        new Municipio(143,"San Miguelito",departamentosData.getDepartamentoById(16)),
        new Municipio(144,"Altagracia",departamentosData.getDepartamentoById(17)),
        new Municipio(145,"Belen",departamentosData.getDepartamentoById(17)),
        new Municipio(146,"Buenos Aires",departamentosData.getDepartamentoById(17)),
        new Municipio(147,"Cardenas",departamentosData.getDepartamentoById(17)),
        new Municipio(148,"Moyogalpa",departamentosData.getDepartamentoById(17)),
        new Municipio(149,"Potosi",departamentosData.getDepartamentoById(17)),
        new Municipio(150,"Rivas",departamentosData.getDepartamentoById(17)),
        new Municipio(151,"San Jorge",departamentosData.getDepartamentoById(17)),
        new Municipio(152,"San Juan del Sur",departamentosData.getDepartamentoById(17)),
        new Municipio(153,"Tola",departamentosData.getDepartamentoById(17))
       };
     
        /* Departamento[] departamentos = departamentosData.getDepartamentos();
        for(Departamento d: departamentos){
            
        }*/
    }
    
    public Municipio[] getMunicipios(){
        return municipios;
    }
    
    public Municipio getMunicipioById(int id){
        return(id <= 0) ? null :(id > municipios.length) ? null : municipios[id-1];
        
    }
    public Municipio getMunicipioByNombre(String name){
        if (name == null){
            return null;
        }
        if (name.isEmpty()){
            return null;
        }
        
        int index = municipiosBinarySearch(name);
        if(index <= -1){
            return null;
        }
        return municipios[index];
    }
   
    private  int municipiosBinarySearch(String key) {
        int index = -1;
        int low = 0, high = municipios.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (municipios[mid].getNombre().compareToIgnoreCase(key) < 0) {
                low = mid + 1;
            } else if (municipios[mid].getNombre().compareToIgnoreCase(key) > 0) {
                high = mid - 1;
            } else if (municipios[mid].getNombre().equalsIgnoreCase(key)) {
                index = mid;
                break;
            }
        }
        return index;
    }
    
    public Municipio[] getMunicipiosByDepartamento(Departamento departamento){
        Municipio municipiosByDepartamento[] = null;
        for(Municipio m : municipios ){
            if(m.getDepartamento().getCodigo()== departamento.getCodigo()){
                municipiosByDepartamento = addMunicipio(municipiosByDepartamento,m);
            }
        }
        
        return municipiosByDepartamento;
    }
    
    private Municipio[] addMunicipio(Municipio municipiosCopy[],Municipio m){
        if(municipiosCopy == null){
            municipiosCopy = new Municipio[1];
            municipiosCopy[0] = m;
        }
        
        municipiosCopy = Arrays.copyOf(municipiosCopy,municipiosCopy.length+1);
        municipiosCopy[municipiosCopy.length-1] = m;
        
        return municipiosCopy;
    }
    

}
