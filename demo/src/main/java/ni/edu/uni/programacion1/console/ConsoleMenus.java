/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

/**
 *
 * @author Sistemas36
 */
public class ConsoleMenus {
    public static void mainMenu(){
        System.out.println("*****Menu de opciones******");
        System.out.println("1-Gestion de empleados");
        System.out.println("2-Reportes de empleados");
        System.out.println("3-Salir");
    }
    
    public static void gestionSubMenu(){
        System.out.println("1-Agregar");
        System.out.println("2-Editar ");
        System.out.println("3-Eleminar");
        System.out.println("4-Regresar");
    }
    
    public static void reportesSubMenu(){
        System.out.println("1-Visualizar por sexo");
        System.out.println("2-Visualizar por NIvel academico");
        System.out.println("3-Visualizar por Municipio");
        System.out.println("4-Visualizar por Departamento");
        System.out.println("5-Visualizar por Cedula");
        System.out.println("6-Visualizar por Apellido");
        System.out.println("7-Buscar por codigo");
        System.out.println("8-Regresar");
    }
    
}
