/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import ni.edu.uni.programacion1.enums.NivelAcademico;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author Sistemas36
 */
public class ConsoleGestionEmpleado {
    public static Empleado readEmpleado(BufferedReader reader) throws IOException{
        System.out.println("A continuacion se le va a solicitar informacion del empleado");
        int codigo = ConsoleReader.readInt(reader, "Codigo");
        String cedula = ConsoleReader.readCedula(reader, "Cedula");
        String primerNombre =ConsoleReader.readLetter(reader, "Primer Nombre");
        String segundoNombre= ConsoleReader.readLetter(reader, "Segundo Nombre");
        String primerApellido = ConsoleReader.readLetter(reader, "Primer Apellido");
        String segundoApellido = ConsoleReader.readLetter(reader,"Sgundo Apellido");
        String direccion = ConsoleReader.readLetter(reader,"Direccion");
        String telefono = ConsoleReader.readTelefono(reader, "Telefono");
        String celular = ConsoleReader.readTelefono(reader,"Celular");
        String sexo = ConsoleReader.readSexo(reader, " Sexo :Sexo: 1. Femenino "+ "2. Masculino\n");
        String nivelAcademico=ConsoleReader.readnivelAcademico(reader, "Nivel Academico: 1. PRIMARIA,\n" +
"    2. BACHILLER,\n" +" 3. TECNICO_MEDIO,\n" +"    4. TECNICO_SUPERIOR,\n" +"    5. LICENCIADO,\n" +
"    6. INGENIERO,\n" +"    7. ESPECIALISTA,\n" +"    8. MAESTRIA,\n" +"    9. DOCTORADO,\n" +"    10. POST_DOCTORADO");
        String municipio=ConsoleReader.readLetter(reader, "Municipio:");
        return new Empleado(codigo, cedula, primerNombre, segundoNombre, primerApellido, segundoApellido, direccion, telefono, celular, sexo, nivelAcademico, municipio);
    } 
    
    
    
}
