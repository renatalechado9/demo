/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.app;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ni.edu.uni.programacion1.console.ConsoleDemo;

/**
 *
 * @author Sistemas36
 */
public class Aplicattion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ConsoleDemo.start();
        } catch (IOException ex) {
            Logger.getLogger(Aplicattion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
