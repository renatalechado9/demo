/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.demo;

import static ni.edu.uni.programacion1.enums.NivelAcademico.INGENIERO;
import static ni.edu.uni.programacion1.enums.Sexo.FEMENINO;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author USUARIO
 */
public class ApplicationDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empleado empleados[] = {
            //new Empleado(),
            //new Empleado(12, "001-110285-0078L", "Ana", "Petronila", "Conda", "Larios", "Km 5", "-", "88557744", FEMENINO, INGENIERO),
            //new Empleado(15, "001-100588-1278K", "Armando", "Jose", "Guerra", "Paz", "Km 5", "-", "78451236")
        };       
     
        //Empleado empleado1 = new Empleado(0, cedula, primerNombre, segundoNombre, primerApellido, segundoApellido, direccion, telefono, celular, FEMENINO, INGENIERO, municipio);
       
        for(Empleado e : empleados){
            printEmpleado(e);
        }
        //for(Empleado e : empleados2){
        //   printEmpleado(e);
        //}
    }
    
    public static void printEmpleado(Empleado e){
        System.out.println("Numero Trabajador: " + e.getCodTrabajador());
        System.out.println("Primer Nombre: " + e.getPrimerNombre());
        System.out.println("Segundo Nombre:" + e.getSegundoNombre());
        System.out.println("Primer Apellido: " + e.getPrimerApellido());
        System.out.println("Segundo Apellido: " + e.getSegundoApellido());
        System.out.println("Cedula: " + e.getCedula());
        System.out.println("Direccion: " + e.getDireccion());
        System.out.println("Telefono: " + e.getTelefono());
        System.out.println("Celular: " + e.getCelular());
        System.out.println("Sexo: " + e.getSexo());
        System.out.println("Nivel Academico: " + e.getNivelAcademico());
    }
        // TODO code application logic here
}
