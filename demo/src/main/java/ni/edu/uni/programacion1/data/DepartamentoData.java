/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import ni.edu.uni.programacion1.pojo.Departamento;

/**
 *
 * @author USUARIO
 */
public class DepartamentoData {
    private Departamento[] departamentos;

    public DepartamentoData() {
        populateDepartamento();
    }

    private void populateDepartamento() {
        departamentos = new Departamento[]{
            new Departamento(1, "Boaco"),
            new Departamento(2, "Carazo"),
            new Departamento(3, "Chinandega"),
            new Departamento(4, "Chonatles"),
            new Departamento(5, "Costa Caribe Norte"),
            new Departamento(6, "Costa CAribe Sur"),
            new Departamento(7, "Esteli"),
            new Departamento(8, "Granada"),
            new Departamento(9, "Jinotega"),
            new Departamento(10, "leon"),
            new Departamento(11, "Madriz"),
            new Departamento(12, "Managua"),
            new Departamento(13, "Masaya"),
            new Departamento(14, "Matagalpa"),
            new Departamento(15, "Nueva Segovia"),
            new Departamento(16, "Rio San Juan"),
            new Departamento(17, "Rivas")};
    }

    public Departamento[] getDepartamentos() {
        return departamentos;
    }
    
    public Departamento getDepartamentoById(int id) {
        return id <= 0 ? null : id > departamentos.length ? null : departamentos[id - 1];
    }

   public   Departamento getDepartamentoByNombre(String nombre) {
        int index = DepartamentoToBinarySearch(nombre);
        if (index < 0) {
            return null;
        }
        return departamentos[index];
    }

    private int DepartamentoToBinarySearch(String key) {
        int index = -1;
        int low = 0, high = departamentos.length - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (departamentos[mid].getNombre().compareToIgnoreCase(key) < 0) {
                low = mid + 1;
            } else if (departamentos[mid].getNombre().compareToIgnoreCase(key) > 0) {
                high = mid - 1;
            } else if (departamentos[mid].getNombre().equalsIgnoreCase(key)) {
                index = mid;
                break;
            }
        }
        return index;
    }
    
}
