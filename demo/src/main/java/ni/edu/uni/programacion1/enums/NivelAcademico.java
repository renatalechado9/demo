/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.enums;

/**
 *
 * @author USUARIO
 */
public enum NivelAcademico {
    PRIMARIA,
    BACHILLER,
    TECNICO_MEDIO,
    TECNICO_SUPERIOR,
    LICENCIADO,
    INGENIERO,
    ESPECIALISTA,
    MAESTRIA,
    DOCTORADO,
    POST_DOCTORADO
}
 