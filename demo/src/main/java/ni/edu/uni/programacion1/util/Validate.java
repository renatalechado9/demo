/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Sistemas36
 */
public class Validate {
    public static boolean validateIsInteger(String text){
        return validate1("^\\d+$",text);
    }
    
    public static boolean validateIsLetters(String text){
        return validate1("/^[a-z ,.'-]+$/i",text);
    }
    
    public static boolean validateIsCedula(String text){
      return validate1("^((\\d{3})-(\\d{6})-(\\d{4})[A-Z])?$",text);
    }
    public static boolean validateIsPhoneNumber(String text){
      return validate1("\\d{8}",text);
    }
    public static boolean validateIsSexo(String text){
      return validate1("^\\d+$",text);         
    }
    private static boolean validate1(String expression,String text){
        Pattern p = Pattern.compile(expression);
        Matcher m = p.matcher(text);
        
        return m.matches();
     
    }
    
}
