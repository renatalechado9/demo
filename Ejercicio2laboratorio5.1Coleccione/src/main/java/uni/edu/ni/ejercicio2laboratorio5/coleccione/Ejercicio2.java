/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.edu.ni.ejercicio2laboratorio5.coleccione;


import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 *La UNI ha decidido que cada nuevo servicio lleve el nombre de un empleado pero solo el
 * primer nombre será usado y cada nombre será usado solo una vez. Prepare una lista de nombres 
 * de empleados únicos
 * @author Sistemas36
 */
public class Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Set <String> empleados= new HashSet<>();
       Scanner md= new Scanner(System.in);
       int opcion, salida=0;
       do{
           do{
                System.out.println("Digite la opcion que usted necesite");
                System.out.println("1. Registrar Empleado");
                System.out.println("2. Mostrar empleados");
                System.out.println("3. Salir");  
                opcion=md.nextInt();
                if(opcion>3 && opcion<1){
                    System.out.println("ERROR");
                    System.out.println("Ponga un numero que este dentro de las variables");
                }else{
                    salida = 1;
                }
            }while(salida==0 );
                switch(opcion){
                    case 1:
                        System.out.println("Digite el nombre que desea agregar");
                        String nombredeempleado = md.next();
                        empleados.add(nombredeempleado);
                        System.out.println("___________________");
                        break;
                    case 2:
                            System.out.println("Empleados: " );
                            System.out.println(empleados);
                            System.out.println("_______________");

                        break;
                    case 3:
                        System.out.println("Byeee");
                        salida=2;
                        break;
                }
    }while(salida!=2);
    
    }
    
}
