/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Daoimpl;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import uni.dao.SucursalDao;
import uni.pojo.Sucursal;

/**
 *
 * @author Sistemas40
 */
public class Sucursalimpl implements SucursalDao{
   private File file;
    private RandomAccessFile raf;
    private String path;
    private final int SIZE = 325;

    public Sucursalimpl() {
        path = "base.dat";
        file = new File(path);
    }

    private void openRAF() throws IOException {
        if (!file.exists()) {
            file.createNewFile();
            raf = new RandomAccessFile(file, "rw");
            raf.seek(0);
            raf.writeInt(0);
            raf.writeInt(0);
        } else {
            raf = new RandomAccessFile(file, "rw");
        }
    }

    private void closeRAF() throws IOException {
        if (raf != null) {
            raf.close();
        }
    }

    public Sucursalimpl(String path) {
        this.path = path;
        file = new File(path);
    }

    public Sucursalimpl(File file) {
        this.file = file;
    } 
   @Override
     public int save(Sucursal t) throws IOException {
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        int k = raf.readInt();
        long pos = 8 + SIZE * n;
        raf.seek(pos);
        raf.writeInt(++k);
        raf.writeUTF(fixString(t.getNombresucursal(), 20));
        raf.writeUTF(fixString(t.getRepresentante(), 20));
        raf.writeUTF(fixString(t.getMunicipio(), 20));
        raf.writeUTF(fixString(t.getDireccion(), 20));
        raf.writeUTF(fixString(t.getTelefono(), 20));
        raf.writeUTF(fixString(t.getCorreo(), 30));
        raf.writeBoolean(t.isActivo());
        raf.writeUTF(fixString(t.getFecha_creacion(), 20));
        raf.writeUTF(fixString(t.getFecha_modificacion(), 20));
        raf.seek(0);
        raf.writeInt(++n);
        raf.writeInt(k);

        closeRAF();
        return 1;
    }

    private String fixString(String text, int capacity) {
        StringBuilder sb = null;
        if (text == null) {
            sb = new StringBuilder(capacity);
        } else {
            sb = new StringBuilder(text);
            sb.setLength(capacity);
        }
        return sb.toString();
    }

   @Override
    public boolean update(Sucursal t) throws IOException {
        int id = t.getId();
        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        if (id <= 0 || id > n) {
            closeRAF();
            return false;
        }
        long pos = 8 + (id - 1) * SIZE;
        raf.seek(pos);
        int code = raf.readInt();//el id no se cambia 
        if (code == id) {
            raf.writeUTF(fixString(t.getNombresucursal(), 20));
        raf.writeUTF(fixString(t.getRepresentante(), 20));
        raf.writeUTF(fixString(t.getMunicipio(), 20));
        raf.writeUTF(fixString(t.getDireccion(), 20));
        raf.writeUTF(fixString(t.getTelefono(), 20));
        raf.writeUTF(fixString(t.getCorreo(), 30));
        raf.writeBoolean(t.isActivo());
        raf.writeUTF(fixString(t.getFecha_modificacion(), 20));
            closeRAF();
            return true;
        }
        closeRAF();
        return false;
    }

    @Override
    public Sucursal FindByID(Sucursal[] t, int id) throws IOException {
        for (Sucursal clien : t) {
            if (id == clien.getId()) {
                return clien;
            }
        }
        return null;
    }

    @Override
    public Sucursal[] findAll() throws IOException {
        Sucursal sucursales[] = null;

        openRAF();
        raf.seek(0);
        int n = raf.readInt();
        Sucursal c;
        for (int i = 0; i < n; i++) {
            long pos = 8 + i * SIZE;
            raf.seek(pos);
            c = new Sucursal();
            c.setId(raf.readInt());
            c.setNombresucursal(raf.readUTF());
            c.setRepresentante(raf.readUTF());
            c.setMunicipio(raf.readUTF());
            c.setDireccion(raf.readUTF());
            c.setTelefono(raf.readUTF());
            c.setCorreo(raf.readUTF());
            c.setActivo(raf.readBoolean());
            c.setFecha_creacion(raf.readUTF());
            c.setFecha_modificacion(raf.readUTF());
        
            sucursales = addElement(sucursales, c);
        }
        closeRAF();
        return sucursales;
    }
     private Sucursal[] addElement(Sucursal sucursales[], Sucursal c) {
        if (sucursales == null) {
            sucursales = new Sucursal[1];
            sucursales[0] = c;
            return sucursales;
        }
        sucursales = Arrays.copyOf(sucursales, sucursales.length + 1);
        sucursales[sucursales.length - 1] = c;
        return sucursales;
    }
    

}
