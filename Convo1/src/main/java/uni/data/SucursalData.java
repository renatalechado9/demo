/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.data;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import uni.pojo.Sucursal;

public class SucursalData {
  private Sucursal[] sucursales;

    public SucursalData() throws FileNotFoundException {
        populateSucursal();
    }
    private void populateSucursal() throws FileNotFoundException{
     Gson gson = new Gson();
     sucursales = gson.fromJson(new FileReader("Sucursal_Data.json"), Sucursal[].class);
     
    
    }
    
    public Sucursal[] getSucursales() {
        return sucursales;
    }
}
