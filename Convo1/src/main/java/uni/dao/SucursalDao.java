/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.dao;

import java.io.IOException;
import uni.pojo.Sucursal;

/**
 *
 * @author Sistemas40
 */
public interface SucursalDao extends Dao<Sucursal>{
        Sucursal FindByID(Sucursal[] t, int id)  throws IOException;
}
