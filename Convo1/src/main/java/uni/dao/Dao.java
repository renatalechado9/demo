/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.dao;

import java.io.IOException;

/**
 *
 * @author Sistemas40
 * @param <T>
 */
public interface Dao <T>{
    int save(T t) throws IOException;

    boolean update(T t) throws IOException;

    T[] findAll() throws IOException;
}
