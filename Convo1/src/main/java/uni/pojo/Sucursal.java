/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.pojo;

/**
 *
 * @author Sistemas40
 */
public class Sucursal {
    private int id;
    private String nombresucursal;
    private String representante;
    private String municipio;
    private String direccion;
    private String telefono;
    private String correo;
    boolean activo;
    private String fecha_creacion;
    private String fecha_modificacion;

    public Sucursal() {
    }

    public Sucursal(int id, String nombresucursal, String representante, String municipio, String direccion, String telefono, String correo, boolean activo, String fecha_creacion, String fecha_modificacion) {
        this.id = id;
        this.nombresucursal = nombresucursal;
        this.representante = representante;
        this.municipio = municipio;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo = correo;
        this.activo = activo;
        this.fecha_creacion = fecha_creacion;
        this.fecha_modificacion = fecha_modificacion;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombresucursal() {
        return nombresucursal;
    }

    public void setNombresucursal(String nombresucursal) {
        this.nombresucursal = nombresucursal;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getFecha_modificacion() {
        return fecha_modificacion;
    }

    public void setFecha_modificacion(String fecha_modificacion) {
        this.fecha_modificacion = fecha_modificacion;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id=" + id + ", nombresucursal=" + nombresucursal + ", representante=" + representante + ", municipio=" + municipio + ", direccion=" + direccion + ", telefono=" + telefono + ", correo=" + correo + ", activo=" + activo + ", fecha_creacion=" + fecha_creacion + ", fecha_modificacion=" + fecha_modificacion + '}';
    }
    
    
}
