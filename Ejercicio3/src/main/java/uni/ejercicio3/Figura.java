/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.ejercicio3;

/**
 *
 * @author Sistemas36
 */
public abstract class Figura {
    private double Pi;
    
    

    public Figura(int Pi){
        this.Pi = Pi;
    }

    public double getPi() {
        return Pi;
    }

    public void setPi(double Pi) {
        this.Pi = Pi;
    }
    public abstract float area();
}
