/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.ejercicio3;

/**
 *
 * @author Sistemas36
 */
public class Rectangulo extends Figura {
    private float base;
    private float altura;
    public Rectangulo(int Pi) {
        super(Pi);
    }

    public Rectangulo(float base, float altura, int Pi) {
        super(Pi);
        this.base = base;
        this.altura = altura;
    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }
public float area(){
    return base*altura;
}
    
    
}
