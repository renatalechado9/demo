/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.ejercicio3;

/**
 *
 * @author Sistemas36
 */
public class Cuadrado extends Figura{
    private int base;
    
    public Cuadrado(int Pi) {
        super(Pi);
    }

    public Cuadrado(int base, int Pi) {
        super(Pi);
        this.base = base;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }
    public float area(){
        return base*base;
    }
    
   
    
}
