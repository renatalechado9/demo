/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.io.ejercicio1y2herenciapolimorfismo;

/**
 *
 * @author Sistemas36
 */
public class Empleado extends Persona {
    private int salariobasico;
    private int id;
    private String cargo;

    public Empleado(String nombres, String apellidos, int edad) {
        super(nombres, apellidos, edad);
    }

    public Empleado(int salariobasico, int id, String cargo, String nombres, String apellidos, int edad) {
        super(nombres, apellidos, edad);
        this.salariobasico = salariobasico;
        this.id = id;
        this.cargo = cargo;
    }

    public float getSalariobasico() {
        return salariobasico;
    }

    public void setSalarioBásico(int salariobasico) {
        this.salariobasico = salariobasico;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Empleado{" + "salarioBasico=" + salariobasico + ", id=" + id + ", cargo=" + cargo +",Nombre="+getNombres()+",=Apellidos"+getApellidos()+",=Edad "+getEdad()+ '}';
    }
    
}
